# frozen_string_literal: true

require_relative 'String'

# Module Tools
module Tools
  def self.sqlite_conn(db_name = 'test.db')
    require 'sqlite3'
    db = SQLite3::Database.new db_name
    db.results_as_hash = true
    db
  end

  def self.sqlite_select(db_name, query, *params)
    db = sqlite_conn db_name
    db.execute(query, params)
  rescue SQLite3::Exception => e
    puts 'Select Exception:'.red + "\n #{e}".yellow
    STDIN.gets
    puts db_name, query, params
    exit 13
  ensure
    db&.close
  end

  def self.sqlite_insert(db_name, query, *params)
    db = sqlite_conn db_name
    db.transaction
    db.execute(query, params)
    db.commit
  rescue SQLite3::Exception => e
    db.rollback
    puts 'Insert Exception:'.red + "\n #{e}".yellow
    STDIN.gets
    puts db_name, query, params
    exit 13
  ensure
    db&.close
  end

  # returns a text based file as array
  def self.read_textfile(path)
    # puts path
    File.open(path, 'r').map(&:strip) if path
  end

  # saves data as a file
  def self.save(data, path)
    f = File.open(path, 'w')
    f.write(data)
    res = "Data saved to: #{path}"
  rescue File::Exception => e
    res = "Save File Exception:\n#{e}"
  ensure
    f&.close
    res
  end

  # saves data as a file with datetime
  def self.save_with_datetime(data, path)
    ext = File.extname(path)
    dt = DateTime.now.strftime('%Y%m%d_%H%M%S')
    path = "#{path.gsub(ext, '')}_#{dt}#{ext}"
    msg = save(data, path)
    puts '=' * msg.size, msg, '=' * msg.size
  end

  # saves a stream as a file
  def self.download_file(url, path = './')
    require 'net/https'
    uri = URI(url)
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |x|
      req = Net::HTTP::Get.new(uri)
      x.request req do |res|
        f = res['content-disposition']
        if res.code.to_i != 200 
          puts "!Attention!\theader: '#{res.header.inspect}'"
          exit 13
        end
        if (File.basename(path) == '.')
          riase 'cant get filename' unless f

          path += f.sub(/^.+filename=(.+);.+$/, '\1').strip
        end
        # puts path
        # break
        # File.open(path + File.basename(uri.path), 'w') do |io|
        File.open(path, 'w') do |io|
          res.read_body { |chunk| io.write chunk }
        end
      end
    end
    path
  end

  # get json response
  def self.get_json(url)
    # ...
    require 'json'
    require 'net/https'
    uri = URI.parse(url)
    res = Net::HTTP.get_response uri
    raise "Tools.jet_json: response fail - '#{res}'" unless res.is_a? Net::HTTPOK

    JSON.parse(res.body)
  end
end
