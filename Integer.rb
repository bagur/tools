# frozen_string_literal: true

# Class enhancer: Integer
class Integer
  # double
  define_method(:double) { self * 2 }
end
