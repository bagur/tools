# frozen_string_literal: true

# def Timer
class Timer
  require 'date'
  attr_reader :started, :timers

  def to_s
    "Started at: #{@started};" \
    "\n    Timers: #{@timers.size};" \
    "\n     Total: #{total}."
  end

  def initialize
    @started = Time.now
    @timers = []
  end

  def show
    return unless @timers.last

    # ...
    puts 'All timers:'
    @timers.each do |item|
      puts "[#{item[:title]}]".rjust(10) + " #{image(item[:begin], item[:end])}"
    end
  end

  def update(title = nil)
    # ...
    title ||= @timers.index(@timers.last).to_i
    ts = @timers.last ? @timers.last[:end] : @started
    te = Time.now
    @timers << { begin: ts, end: te, title: title }
    puts "Timer[#{title}] -" + " #{image(ts, te)}."
  end

  def total
    df = Time.now
    image(@started, df)
  end

  private

  def image(start, finish)
    dt = (finish - start).to_i
    timer = { hours: dt / 3600, minutes: (dt % 3600) / 60, seconds: dt % 60 }
    format("%<hours>02d:%<minutes>02d:%<seconds>02d[#{dt}]", timer)
  end
end
